package com.wh.client;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

import javax.xml.namespace.QName;


public class WebServiceClient {


    public static void main(String[] args) {
//        RPCServiceClient r;
    }

    private static Object[] requestFunctionWebService(String url,
                                                      String parameters,
                                                      String namespance,String methodName) {
        try {
            RPCServiceClient rpcServiceClient = new RPCServiceClient();
            EndpointReference endpointReference = new EndpointReference(url);//url后缀不加"？wsdl"
            Options options = rpcServiceClient.getOptions();
            options.setTimeOutInMilliSeconds(30000);
            options.setProperty(HTTPConstants.SO_TIMEOUT, 30000);
            options.setTo(endpointReference);
            rpcServiceClient.setOptions(options);
            QName qName = new QName(namespance, methodName);
            Class[] classStr = new Class[] {String.class};
            Object[] params = new Object[] {parameters};
            Object[] objects = rpcServiceClient.invokeBlocking(qName,params,classStr);
            return objects;
        } catch (Exception e) {
            return null;
        }
    }
//    public static Object[] invokeRemoteMethod(String url, String method, Object[] parameters) {
//        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//        if (!url.endsWith("wsdl")) {
//            url += "?wsdl";
//        }
//        org.apache.cxf.endpoint.Client client = dcf.createClient(url);
//        try {
//            Object[] objects = client.invoke(method, parameters);
//            return objects;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }


}
