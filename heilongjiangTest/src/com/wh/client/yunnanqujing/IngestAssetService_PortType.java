/**
 * IngestAssetService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.yunnanqujing;

public interface IngestAssetService_PortType extends java.rmi.Remote {
    public com.wh.client.yunnanqujing.IngestAssetResponse ingestAssetDeal(com.wh.client.yunnanqujing.IngestAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing.QueryStateResponse queryStateDeal(com.wh.client.yunnanqujing.QueryState parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing.UnpublishResponse unpublishDeal(com.wh.client.yunnanqujing.Unpublish parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing.DeleteAssetResponse deleteAssetDeal(com.wh.client.yunnanqujing.DeleteAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing.PublishResponse publishDeal(com.wh.client.yunnanqujing.Publish parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing.BindProductResponse bindProductDeal(com.wh.client.yunnanqujing.BindProduct parameters) throws java.rmi.RemoteException;
}
