/**
 * IngestAssetService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.localYuNanQuJing;

public interface IngestAssetService_PortType extends java.rmi.Remote {
    public com.wh.client.localYuNanQuJing.IngestAssetResponse ingestAssetDeal(com.wh.client.localYuNanQuJing.IngestAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.localYuNanQuJing.QueryStateResponse queryStateDeal(com.wh.client.localYuNanQuJing.QueryState parameters) throws java.rmi.RemoteException;
    public com.wh.client.localYuNanQuJing.UnpublishResponse unpublishDeal(com.wh.client.localYuNanQuJing.Unpublish parameters) throws java.rmi.RemoteException;
    public com.wh.client.localYuNanQuJing.DeleteAssetResponse deleteAssetDeal(com.wh.client.localYuNanQuJing.DeleteAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.localYuNanQuJing.PublishResponse publishDeal(com.wh.client.localYuNanQuJing.Publish parameters) throws java.rmi.RemoteException;
    public com.wh.client.localYuNanQuJing.BindProductResponse bindProductDeal(com.wh.client.localYuNanQuJing.BindProduct parameters) throws java.rmi.RemoteException;
}
