/**
 * IngestNotifyService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.localYuNanQuJing;

public class IngestNotifyService_ServiceLocator extends org.apache.axis.client.Service implements com.wh.client.localYuNanQuJing.IngestNotifyService_Service {

    public IngestNotifyService_ServiceLocator() {
    }


    public IngestNotifyService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IngestNotifyService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IngestNotifyServiceSOAP
    private java.lang.String IngestNotifyServiceSOAP_address = "http://www.example.org/";

    public java.lang.String getIngestNotifyServiceSOAPAddress() {
        return IngestNotifyServiceSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IngestNotifyServiceSOAPWSDDServiceName = "IngestNotifyServiceSOAP";

    public java.lang.String getIngestNotifyServiceSOAPWSDDServiceName() {
        return IngestNotifyServiceSOAPWSDDServiceName;
    }

    public void setIngestNotifyServiceSOAPWSDDServiceName(java.lang.String name) {
        IngestNotifyServiceSOAPWSDDServiceName = name;
    }

    public com.wh.client.localYuNanQuJing.IngestNotifyService_PortType getIngestNotifyServiceSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IngestNotifyServiceSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIngestNotifyServiceSOAP(endpoint);
    }

    public com.wh.client.localYuNanQuJing.IngestNotifyService_PortType getIngestNotifyServiceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.wh.client.localYuNanQuJing.IngestNotifyServiceSOAPStub _stub = new com.wh.client.localYuNanQuJing.IngestNotifyServiceSOAPStub(portAddress, this);
            _stub.setPortName(getIngestNotifyServiceSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIngestNotifyServiceSOAPEndpointAddress(java.lang.String address) {
        IngestNotifyServiceSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.wh.client.localYuNanQuJing.IngestNotifyService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.wh.client.localYuNanQuJing.IngestNotifyServiceSOAPStub _stub = new com.wh.client.localYuNanQuJing.IngestNotifyServiceSOAPStub(new java.net.URL(IngestNotifyServiceSOAP_address), this);
                _stub.setPortName(getIngestNotifyServiceSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IngestNotifyServiceSOAP".equals(inputPortName)) {
            return getIngestNotifyServiceSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://IngestNotifyService.homed.ipanel.cn", "IngestNotifyService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://IngestNotifyService.homed.ipanel.cn", "IngestNotifyServiceSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IngestNotifyServiceSOAP".equals(portName)) {
            setIngestNotifyServiceSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
