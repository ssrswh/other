/**
 * IngestNotifyService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.localYuNanQuJing;

public interface IngestNotifyService_Service extends javax.xml.rpc.Service {
    public java.lang.String getIngestNotifyServiceSOAPAddress();

    public com.wh.client.localYuNanQuJing.IngestNotifyService_PortType getIngestNotifyServiceSOAP() throws javax.xml.rpc.ServiceException;

    public com.wh.client.localYuNanQuJing.IngestNotifyService_PortType getIngestNotifyServiceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
