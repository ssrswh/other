/**
 * IngestNotifyService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.localYuNanQuJing;

public interface IngestNotifyService_PortType extends java.rmi.Remote {
    public com.wh.client.localYuNanQuJing.IngestNotifyResponse ingestNotifyDeal(com.wh.client.localYuNanQuJing.IngestNotify notifyReq) throws java.rmi.RemoteException;
    public com.wh.client.localYuNanQuJing.PublishNotifyResponse publishNotifyDeal(com.wh.client.localYuNanQuJing.PublishNotify notifyReq) throws java.rmi.RemoteException;
}
