/**
 * IngestAssetService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.yunnanqujing2;

public interface IngestAssetService_Service extends javax.xml.rpc.Service {
    public String getIngestAssetServiceSOAPAddress();

    public com.wh.client.yunnanqujing2.IngestAssetService_PortType getIngestAssetServiceSOAP() throws javax.xml.rpc.ServiceException;

    public com.wh.client.yunnanqujing2.IngestAssetService_PortType getIngestAssetServiceSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
