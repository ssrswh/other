/**
 * IngestAssetService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.yunnanqujing2;

public interface IngestAssetService_PortType extends java.rmi.Remote {
    public com.wh.client.yunnanqujing2.IngestAssetResponse ingestAssetDeal(com.wh.client.yunnanqujing2.IngestAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing2.QueryStateResponse queryStateDeal(com.wh.client.yunnanqujing2.QueryState parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing2.UnpublishResponse unpublishDeal(com.wh.client.yunnanqujing2.Unpublish parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing2.DeleteAssetResponse deleteAssetDeal(com.wh.client.yunnanqujing2.DeleteAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing2.PublishResponse publishDeal(com.wh.client.yunnanqujing2.Publish parameters) throws java.rmi.RemoteException;
    public com.wh.client.yunnanqujing2.BindProductResponse bindProductDeal(com.wh.client.yunnanqujing2.BindProduct parameters) throws java.rmi.RemoteException;
}
