/**
 * IngestAssetServiceSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.yunnanqujing2;

public class IngestAssetServiceSOAPStub extends org.apache.axis.client.Stub implements com.wh.client.yunnanqujing2.IngestAssetService_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[6];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("IngestAssetDeal");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "ing:IngestAsset"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">IngestAsset"), com.wh.client.yunnanqujing2.IngestAsset.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">IngestAssetResponse"));
        oper.setReturnClass(com.wh.client.yunnanqujing2.IngestAssetResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "IngestAssetResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryStateDeal");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "ing:QueryState"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">QueryState"), com.wh.client.yunnanqujing2.QueryState.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">QueryStateResponse"));
        oper.setReturnClass(com.wh.client.yunnanqujing2.QueryStateResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "QueryStateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UnpublishDeal");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "ing:Unpublish"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">Unpublish"), com.wh.client.yunnanqujing2.Unpublish.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">UnpublishResponse"));
        oper.setReturnClass(com.wh.client.yunnanqujing2.UnpublishResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "UnpublishResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteAssetDeal");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "ing:DeleteAsset"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">DeleteAsset"), com.wh.client.yunnanqujing2.DeleteAsset.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">DeleteAssetResponse"));
        oper.setReturnClass(com.wh.client.yunnanqujing2.DeleteAssetResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "DeleteAssetResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PublishDeal");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "ing:Publish"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">Publish"), com.wh.client.yunnanqujing2.Publish.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">PublishResponse"));
        oper.setReturnClass(com.wh.client.yunnanqujing2.PublishResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "PublishResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BindProductDeal");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "ing:BindProduct"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">BindProduct"), com.wh.client.yunnanqujing2.BindProduct.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">BindProductResponse"));
        oper.setReturnClass(com.wh.client.yunnanqujing2.BindProductResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", "BindProductResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

    }

    public IngestAssetServiceSOAPStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public IngestAssetServiceSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public IngestAssetServiceSOAPStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">BindProduct");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.BindProduct.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">BindProductResponse");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.BindProductResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">DeleteAsset");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.DeleteAsset.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">DeleteAssetResponse");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.DeleteAssetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">IngestAsset");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.IngestAsset.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">IngestAssetResponse");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.IngestAssetResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">Publish");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.Publish.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">PublishResponse");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.PublishResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">QueryState");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.QueryState.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">QueryStateResponse");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.QueryStateResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">Unpublish");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.Unpublish.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://IngestAssetService.homed.ipanel.cn", ">UnpublishResponse");
            cachedSerQNames.add(qName);
            cls = com.wh.client.yunnanqujing2.UnpublishResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        Class cls = (Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            Class sf = (Class)
                                 cachedSerFactories.get(i);
                            Class df = (Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.wh.client.yunnanqujing2.IngestAssetResponse ingestAssetDeal(com.wh.client.yunnanqujing2.IngestAsset parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "IngestAssetDeal"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.wh.client.yunnanqujing2.IngestAssetResponse) _resp;
            } catch (Exception _exception) {
                return (com.wh.client.yunnanqujing2.IngestAssetResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.wh.client.yunnanqujing2.IngestAssetResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.wh.client.yunnanqujing2.QueryStateResponse queryStateDeal(com.wh.client.yunnanqujing2.QueryState parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "QueryStateDeal"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.wh.client.yunnanqujing2.QueryStateResponse) _resp;
            } catch (Exception _exception) {
                return (com.wh.client.yunnanqujing2.QueryStateResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.wh.client.yunnanqujing2.QueryStateResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.wh.client.yunnanqujing2.UnpublishResponse unpublishDeal(com.wh.client.yunnanqujing2.Unpublish parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UnpublishDeal"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.wh.client.yunnanqujing2.UnpublishResponse) _resp;
            } catch (Exception _exception) {
                return (com.wh.client.yunnanqujing2.UnpublishResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.wh.client.yunnanqujing2.UnpublishResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.wh.client.yunnanqujing2.DeleteAssetResponse deleteAssetDeal(com.wh.client.yunnanqujing2.DeleteAsset parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteAssetDeal"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.wh.client.yunnanqujing2.DeleteAssetResponse) _resp;
            } catch (Exception _exception) {
                return (com.wh.client.yunnanqujing2.DeleteAssetResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.wh.client.yunnanqujing2.DeleteAssetResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.wh.client.yunnanqujing2.PublishResponse publishDeal(com.wh.client.yunnanqujing2.Publish parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "PublishDeal"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.wh.client.yunnanqujing2.PublishResponse) _resp;
            } catch (Exception _exception) {
                return (com.wh.client.yunnanqujing2.PublishResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.wh.client.yunnanqujing2.PublishResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.wh.client.yunnanqujing2.BindProductResponse bindProductDeal(com.wh.client.yunnanqujing2.BindProduct parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "BindProductDeal"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.wh.client.yunnanqujing2.BindProductResponse) _resp;
            } catch (Exception _exception) {
                return (com.wh.client.yunnanqujing2.BindProductResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.wh.client.yunnanqujing2.BindProductResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
