/**
 * IngestAssetService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.kunshanSP;

public interface IngestAssetService_PortType extends java.rmi.Remote {
    public com.wh.client.kunshanSP.IngestAssetResponse ingestAssetDeal(com.wh.client.kunshanSP.IngestAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.kunshanSP.QueryStateResponse queryStateDeal(com.wh.client.kunshanSP.QueryState parameters) throws java.rmi.RemoteException;
    public com.wh.client.kunshanSP.UnpublishResponse unpublishDeal(com.wh.client.kunshanSP.Unpublish parameters) throws java.rmi.RemoteException;
    public com.wh.client.kunshanSP.DeleteAssetResponse deleteAssetDeal(com.wh.client.kunshanSP.DeleteAsset parameters) throws java.rmi.RemoteException;
    public com.wh.client.kunshanSP.PublishResponse publishDeal(com.wh.client.kunshanSP.Publish parameters) throws java.rmi.RemoteException;
    public com.wh.client.kunshanSP.BindProductResponse bindProductDeal(com.wh.client.kunshanSP.BindProduct parameters) throws java.rmi.RemoteException;
}
