/**
 * CPImportServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.heilongjiang;

public class CPImportServiceImplServiceLocator extends org.apache.axis.client.Service implements com.wh.client.heilongjiang.CPImportServiceImplService {

    public CPImportServiceImplServiceLocator() {
    }


    public CPImportServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CPImportServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for cpImportServicePort
    private java.lang.String cpImportServicePort_address = "http://10.20.30.43:8080/adi/services/cpImportService";

    public java.lang.String getcpImportServicePortAddress() {
        return cpImportServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String cpImportServicePortWSDDServiceName = "cpImportServicePort";

    public java.lang.String getcpImportServicePortWSDDServiceName() {
        return cpImportServicePortWSDDServiceName;
    }

    public void setcpImportServicePortWSDDServiceName(java.lang.String name) {
        cpImportServicePortWSDDServiceName = name;
    }

    public com.wh.client.heilongjiang.CpImportService getcpImportServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(cpImportServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getcpImportServicePort(endpoint);
    }

    public com.wh.client.heilongjiang.CpImportService getcpImportServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.wh.client.heilongjiang.CPImportServiceImplServiceSoapBindingStub _stub = new com.wh.client.heilongjiang.CPImportServiceImplServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getcpImportServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setcpImportServicePortEndpointAddress(java.lang.String address) {
        cpImportServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.wh.client.heilongjiang.CpImportService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.wh.client.heilongjiang.CPImportServiceImplServiceSoapBindingStub _stub = new com.wh.client.heilongjiang.CPImportServiceImplServiceSoapBindingStub(new java.net.URL(cpImportServicePort_address), this);
                _stub.setPortName(getcpImportServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("cpImportServicePort".equals(inputPortName)) {
            return getcpImportServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://vms.sobey.com", "CPImportServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://vms.sobey.com", "cpImportServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("cpImportServicePort".equals(portName)) {
            setcpImportServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
