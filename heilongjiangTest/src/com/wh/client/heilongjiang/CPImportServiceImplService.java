/**
 * CPImportServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.heilongjiang;

public interface CPImportServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getcpImportServicePortAddress();

    public com.wh.client.heilongjiang.CpImportService getcpImportServicePort() throws javax.xml.rpc.ServiceException;

    public com.wh.client.heilongjiang.CpImportService getcpImportServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
