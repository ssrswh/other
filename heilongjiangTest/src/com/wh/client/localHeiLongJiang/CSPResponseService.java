/**
 * CSPResponseService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.wh.client.localHeiLongJiang;

public interface CSPResponseService extends javax.xml.rpc.Service {
    public java.lang.String getctmsAddress();

    public com.wh.client.localHeiLongJiang.CSPResponse getctms() throws javax.xml.rpc.ServiceException;

    public com.wh.client.localHeiLongJiang.CSPResponse getctms(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
