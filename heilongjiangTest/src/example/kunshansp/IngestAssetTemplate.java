package example.kunshansp;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class IngestAssetTemplate {



    public static void main(String[] args) {
        System.out.println("进入方法");
        Map<String, String> map = new HashMap<String, String>();
        //拼接xml请求,带有请求头

        String soapRequestData = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ing=\"http://IngestAssetService.homed.ipanel.cn\">\n" +
                "\t<soapenv:Header/>\n" +
                "\t<soapenv:Body>\n" +
                "\t\t<ing:IngestAsset>\n" +
                "\t\t\t<LSPID>JYCM</LSPID>\n" +
                "\t\t\t<AMSID>HomedNingxia</AMSID>\n" +
                "\t\t\t<Sequence>JYCM201711233374d</Sequence>\n" +
                "\t\t\t<FtpPath>ftp://root:root@192.168.130.223:2100/space/20085/20105/JYCM201801010523/</FtpPath>\n" +
                "\t\t\t<AdiFileName>JYCM201711233374.xml</AdiFileName>\n" +
                "\t\t\t<NotifyUrl>http://192.168.130.223:8088/NxVod1/services/IngestNotifyServiceSOAP?WSDL</NotifyUrl>\n" +
                "\t\t</ing:IngestAsset>\n" +
                "\t</soapenv:Body>\n" +
                "</soapenv:Envelope>";

        try {
            String method = "http://172.18.240.112:35006/ingest_asset_service";//比如http://192.177.222.222:8888/services/Service_Name/Function_Name
            PostMethod postMethod = new PostMethod(method);
            byte[] b = soapRequestData.getBytes("utf-8");
            InputStream is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity re = new InputStreamRequestEntity(is, b.length, "application/soap+xml; charset=utf-8");
//            postMethod.addRequestHeader("SOAPAction","");

            postMethod.setRequestEntity(re);
            HttpClient httpClient = new HttpClient();
            int statusCode = httpClient.executeMethod(postMethod);
            //200说明正常返回数据
            if (statusCode != 200) {
                //internet error
            }
            System.out.println(statusCode);
            soapRequestData = postMethod.getResponseBodyAsString();
            System.out.println(soapRequestData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
