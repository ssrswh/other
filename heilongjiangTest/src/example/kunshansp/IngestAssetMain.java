package example.kunshansp;


import com.wh.client.kunshanSP.IngestAsset;
import com.wh.client.kunshanSP.IngestAssetResponse;
import com.wh.client.kunshanSP.IngestAssetService_PortType;
import com.wh.client.kunshanSP.IngestAssetService_ServiceLocator;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Arrays;

public class IngestAssetMain {



    public static void main(String[] argv) {
        System.out.println(Arrays.toString(argv));
        String url = "http://172.18.240.112:35006/ingest_asset_service";
        if(argv!=null && argv.length>0){
            url = argv[0];
        }
        System.out.println("执行请求：url: "+ url);
        try {
            IngestAssetService_ServiceLocator locator = new IngestAssetService_ServiceLocator();
            IngestAssetService_PortType service = locator.getIngestAssetServiceSOAP(new URL(url));
            String LSPID = "JYCM";
            String AMSID = "HomedNingxia";
            String sequence = "JYCM201711233374d";
            String adiFileName = "JYCM201711233374.xml";
            String notifyUrl = "http://192.168.130.223:8088/NxVod1/services/IngestNotifyServiceSOAP?WSDL";
            String ftpPath = "ftp://root:root@192.168.130.223:2100/space/20085/20105/JYCM201801010523/";

            IngestAsset ingestAsset  = new IngestAsset();
            ingestAsset.setAdiFileName(adiFileName);
            ingestAsset.setAMSID(AMSID);
            ingestAsset.setFtpPath(ftpPath);
            ingestAsset.setLSPID(LSPID);
            ingestAsset.setNotifyUrl(notifyUrl);
            ingestAsset.setSequence(sequence);
            IngestAssetResponse response = service.ingestAssetDeal(ingestAsset);
            System.out.println(response.getResultCode());

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


}
