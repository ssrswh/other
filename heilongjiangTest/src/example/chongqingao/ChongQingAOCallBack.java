package example.chongqingao;

import com.wh.server.chongqingao.CSPResponseServiceLocator;
import com.wh.server.chongqingao.CSPResult;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

public class ChongQingAOCallBack {


    public static void main(String[] args) throws ServiceException, RemoteException {
        String cspId = args[0];
        String lspId = args[1];
        int status = Integer.parseInt(args[2]);
        String sid = args[3];
        String ftpPath = args[4];
        CSPResponseServiceLocator local = new CSPResponseServiceLocator();

        CSPResult cSPResult = local.getctms()
                .resultNotify(cspId,lspId,sid,status,ftpPath);
        System.out.println(String.format("重庆AO VOD发布返回参数{correlateID: %s, 状态result: %s, 成功或失败信息: %s",
                sid, cSPResult.getResult(), cSPResult.getErrorDescription()));

    }
}
