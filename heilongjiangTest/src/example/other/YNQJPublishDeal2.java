package example.other;

import com.wh.client.yunnanqujing2.IngestAssetService_PortType;
import com.wh.client.yunnanqujing2.IngestAssetService_ServiceLocator;
import com.wh.client.yunnanqujing2.Publish;
import com.wh.client.yunnanqujing2.PublishResponse;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

public class YNQJPublishDeal2 {


    public static void main(String[] argv) {
        try {
            IngestAssetService_ServiceLocator locator = new IngestAssetService_ServiceLocator();
            IngestAssetService_PortType service = locator.getIngestAssetServiceSOAP(new URL("http://10.142.2.23:35006/ingest_asset_service"));
            System.out.println("执行请求，请求Url：http://10.142.2.23:35006/ingest_asset_service");
            String LSPID = "WASUEDU";
            String AMSID = "YNQJ";
            String sequence = "CP23010020191223097806";
            //云南曲靖 publish 请求参数：Url:http://10.142.2.23:35006/ingest_asset_service/,LSPID:WASUEDU,AMSID:Homed,sequence:CP23010020191225142605,
            // assetId:CP23010020191225142605,ProviderID:WASUEDU,ColumnId:1984,PPVId:199
            Publish publish  = new Publish();
            publish.setAMSID(AMSID);
            publish.setAssetId("CP23010020191223097806");
            publish.setColumnId("1984");
            publish.setLSPID(LSPID);
            publish.setPPVId("199");
            publish.setProviderID("WASUEDU");
            publish.setSequence(sequence);

            PublishResponse response = service.publishDeal(publish);
            System.out.println(response.getResultCode());
            System.out.println(response.getResultMsg());

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
