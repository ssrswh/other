package example.other;

import com.wh.client.yunnanqujing.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

@Slf4j
public class YNQJBindProduct {


    public static void main(String[] argv) {
        try {
            IngestAssetService_ServiceLocator locator = new IngestAssetService_ServiceLocator();
            IngestAssetService_PortType service = locator.getIngestAssetServiceSOAP(new URL("http://10.142.2.23:35006/ingest_asset_service"));
            System.out.println("执行请求，请求Url：http://10.142.2.23:35006/ingest_asset_service");
            String LSPID = "WASUEDU";
            String AMSID = "YNQJ";
            String sequence = "CP23010020191230043702";
            //云南曲靖 publish 请求参数：Url:http://10.142.2.23:35006/ingest_asset_service/,LSPID:WASUEDU,AMSID:Homed,sequence:CP23010020191225142605,
            // assetId:CP23010020191225142605,ProviderID:WASUEDU,ColumnId:1984,PPVId:199
            BindProduct bindProduct  = new BindProduct();
            bindProduct.setAMSID(AMSID);
            bindProduct.setLSPID(LSPID);
            // “Add”首次绑定，“Update”更新。
            //没有删除的关系，更新会把之前的绑定关系清除，然后再设置新的绑定关系。
            bindProduct.setOperate("Add");
            bindProduct.setPlayId("100016706");
            bindProduct.setPPVId("4212000018");
            bindProduct.setSequence(sequence);
            BindProductResponse response = service.bindProductDeal(bindProduct);
            System.out.println("调用 bindProductDeal 返回参数 code "+response.getResultCode()+"  返回提示语："+response.getResultMsg());

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
