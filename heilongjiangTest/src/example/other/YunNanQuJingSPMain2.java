package example.other;

import com.wh.client.yunnanqujing.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

public class YunNanQuJingSPMain2 {


    public static void main(String[] argv) {
        try {
            IngestAssetService_ServiceLocator locator = new IngestAssetService_ServiceLocator();
            IngestAssetService_PortType service = locator.getIngestAssetServiceSOAP(new URL("http://10.142.2.23:35006/ingest_asset_service"));
            System.out.println("执行请求，请求Url：http://10.142.2.23:35006/");
            String LSPID = "WASUEDU";
            String AMSID = "YNQJ";
            String sequence = "CP23010020191204086505";
            String adiFileName = "YunNanQuJingSP_CP23010020191204086505_2019120413520236.xml";
            String notifyUrl = "http://10.255.2.105:8080/adi-vod/services/YunNanQuJing?wsdl";
            String ftpPath = "ftp://wasuftp:wasu123@10.255.2.105:21/ceshi/";

            Publish publish  = new Publish();
            publish.setAMSID(AMSID);
            publish.setAssetId("CP23010020191109012207");
            publish.setColumnId("1984");
            publish.setLSPID(LSPID);
            publish.setPPVId("199");
            publish.setProviderID("WASUEDU");
            publish.setSequence(sequence);

            PublishResponse response = service.publishDeal(publish);
            System.out.println(response.getResultCode());

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
