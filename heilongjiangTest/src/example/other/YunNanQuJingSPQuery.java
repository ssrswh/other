package example.other;

import com.wh.client.yunnanqujing.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

public class YunNanQuJingSPQuery {


    public static void main(String[] argv) {
        try {
            IngestAssetService_ServiceLocator locator = new IngestAssetService_ServiceLocator();
            IngestAssetService_PortType service = locator.getIngestAssetServiceSOAP(new URL("http://10.142.2.23:35006/queryState"));
            String url = "http://10.142.2.23:35000";
            String LSPID = "WASUEDU";
            String AMSID = "YNQJ";
            String sequence = "CP23010020191204086505";
            String adiFileName = "YunNanQuJingSP_CP23010020191204086505_2019120413520236.xml";
            String notifyUrl = "http://10.255.2.105:8080/adi-vod/services/YunNanQuJing?wsdl";
            String ftpPath = "ftp://wasuftp:wasu123@10.255.2.105:21/ceshi/";

            QueryState queryState  = new QueryState();
            queryState.setAMSID(AMSID);
            queryState.setLSPID(LSPID);
            queryState.setSequence(sequence);
            queryState.setAssetId("CP23010020191204086505");
            QueryStateResponse response = service.queryStateDeal(queryState);
            System.out.println(response.getStateCode());

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
