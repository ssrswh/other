package example.other;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class HttpTest2 {

    public static void main(String[] args) {
        System.out.println("进入方法");
        Map<String, String> map = new HashMap<String, String>();
        //拼接xml请求,带有请求头
        // ftp://hljyy:wasu@2019@10.255.56.3:21/test/
        String fipUrl = "ftp://hljyy:wasu2019@10.20.30.50:21/test/add_HeiLongJiangXMT_CP23010020191112068109_4f4268db780849438608704ff7048f16.xml";

        String soapRequestData = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ing=\"http://IngestNotifyService.homed.ipanel.cn/\">\n" +
                "\t<soapenv:Header/>\n" +
                "\t<soapenv:Body>\n" +
                "\t\t<IngestNotify>\n" +
                "\t\t\t<LSPID>CCTV</LSPID>\n" +
                "\t\t\t<AMSID>Homed</AMSID>\n" +
                "\t\t\t<Sequence>6720180503173800</Sequence>\n" +
                "\t\t\t<AssetId>CCTV6720180503173800</AssetId>\n" +
                "\t\t\t<PlayId>100064799</PlayId>\n" +
                "\t\t\t<ResultCode>0</ResultCode>\n" +
                "\t\t\t<ResultMsg>success</ResultMsg>\n" +
                "\t\t</IngestNotify>\n" +
                "\t</soapenv:Body>\n" +
                "\t<soapenv:Header />\n" +
                "</soapenv:Envelope>";

        try {
//            String method = "http://10.20.30.43:8080/adi/services/cpImportService?wsdl";//比如http://192.177.222.222:8888/services/Service_Name/Function_Name
//            String method = "http://10.255.56.2:8080/adi-vod/services/heilongjianggd?wsdl";//比如http://192.177.222.222:8888/services/Service_Name/Function_Name
            String method = "http://10.255.2.105:8080/adi-vod/services/YunNanQuJing?wsdl";//比如http://192.177.222.222:8888/services/Service_Name/Function_Name
            PostMethod postMethod = new PostMethod(method);
            byte[] b = soapRequestData.getBytes("utf-8");
            InputStream is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity re = new InputStreamRequestEntity(is, b.length, "application/soap+xml; charset=utf-8");
//            postMethod.addRequestHeader("SOAPAction","");

            postMethod.setRequestEntity(re);
            HttpClient httpClient = new HttpClient();
            int statusCode = httpClient.executeMethod(postMethod);
            //200说明正常返回数据
            if (statusCode != 200) {
                //internet error
                System.out.println(statusCode);
            }
            soapRequestData = postMethod.getResponseBodyAsString();
            System.out.println(soapRequestData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

