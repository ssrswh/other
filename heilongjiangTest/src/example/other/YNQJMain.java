package example.other;



import com.wh.client.ynqj.IngestAssetService_PortType;
import com.wh.client.ynqj.IngestAssetService_ServiceLocator;

import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Arrays;

public class YNQJMain {


    public static void main(String[] argv) {
        System.out.println(Arrays.toString(argv));
        String url = "http://10.142.2.23:35006/ingest_asset_service";
        if(argv!=null && argv.length>0){
            url = argv[0];
        }
        System.out.println("执行请求：url: "+ url);
        try {
            IngestAssetService_ServiceLocator locator = new IngestAssetService_ServiceLocator();
            IngestAssetService_PortType service = locator.getIngestAssetServiceSOAP(new URL(url));
            String LSPID = "WASUEDU";
            String AMSID = "YNQJ";
            String sequence = "CP23010020191204086505";
            String adiFileName = "YunNanQuJingSP_CP23010020191204086505_2019120413520236.xml";
            String notifyUrl = "http://10.255.2.105:8080/adi-vod/services/YunNanQuJing?wsdl";
            String ftpPath = "ftp://wasuftp:wasu123@10.255.2.105:21/ceshi/";


            int code = -1;
            String msg = "";
            service.ingestAsset(new StringHolder(LSPID),new StringHolder(AMSID)
                    ,new StringHolder(sequence),ftpPath,adiFileName,notifyUrl,new IntHolder(code), new StringHolder(msg));
            System.out.println(code);

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
