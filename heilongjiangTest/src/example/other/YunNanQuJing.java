package example.other;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class YunNanQuJing {

    public static void main(String[] args) {
        System.out.println("进入方法");
        Map<String, String> map = new HashMap<String, String>();
        //拼接xml请求,带有请求头

        String soapRequestData = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ing=\"http://IngestNotifyService.homed.ipanel.cn/\">\n" +
                "\t<soapenv:Body>\n" +
                "\t\t<ing:PublishNotify>\n" +
                "\t\t\t<LSPID>CCTV</LSPID>\n" +
                "\t\t\t<AMSID>Homed</AMSID>\n" +
                "\t\t\t<Sequence>4020180504171200</Sequence>\n" +
                "\t\t\t<AssetId>CCTV4020180504171200</AssetId>\n" +
                "\t\t\t<PlayId>100065139</PlayId>\n" +
                "\t\t</ing:PublishNotify>\n" +
                "\t</soapenv:Body>\n" +
                "\t<soapenv:Header />\n" +
                "</soapenv:Envelope>";

        try {
            String method = "http://10.142.2.23:35006/ingest_asset_service/";//比如http://192.177.222.222:8888/services/Service_Name/Function_Name
            PostMethod postMethod = new PostMethod(method);
            byte[] b = soapRequestData.getBytes("utf-8");
            InputStream is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity re = new InputStreamRequestEntity(is, b.length, "application/soap+xml; charset=utf-8");
//            postMethod.addRequestHeader("SOAPAction","");

            postMethod.setRequestEntity(re);
            HttpClient httpClient = new HttpClient();
            int statusCode = httpClient.executeMethod(postMethod);
            //200说明正常返回数据
            if (statusCode != 200) {
                //internet error
                System.out.println(statusCode);
            }
            soapRequestData = postMethod.getResponseBodyAsString();
            System.out.println(soapRequestData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

