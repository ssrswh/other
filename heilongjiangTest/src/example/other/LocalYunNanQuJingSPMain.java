package example.other;



import com.wh.client.localYuNanQuJing.IngestNotify;
import com.wh.client.localYuNanQuJing.IngestNotifyResponse;
import com.wh.client.localYuNanQuJing.IngestNotifyService_PortType;
import com.wh.client.localYuNanQuJing.IngestNotifyService_ServiceLocator;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Arrays;

public class LocalYunNanQuJingSPMain {


    public static void main(String[] argv) {
        System.out.println(Arrays.toString(argv));
        String url = "http://10.255.2.105:8080/adi-vod/services/YunNanQuJing?wsdl";
        if(argv!=null && argv.length>0){
            url = argv[0];
        }
        System.out.println("执行请求：url: "+ url);
        try {
            IngestNotifyService_ServiceLocator locator = new IngestNotifyService_ServiceLocator();
            IngestNotifyService_PortType service = locator.getIngestNotifyServiceSOAP(new URL(url));
            String LSPID = "WASUEDU";
            String AMSID = "YNQJ";
            String sequence = "CP23010020191204086505";
            String adiFileName = "YunNanQuJingSP_CP23010020191204086505_2019120413520236.xml";
            String notifyUrl = "http://10.255.2.105:8080/adi-vod/services/YunNanQuJing?wsdl";
            String ftpPath = "ftp://wasuftp:wasu123@10.255.2.105:21/ceshi/";


            IngestNotify ingestNotify = new IngestNotify();
            ingestNotify.setAMSID(AMSID);
            ingestNotify.setLSPID(LSPID);
            ingestNotify.setPlayId("123214");
            ingestNotify.setResultCode(100);
            ingestNotify.setResultMsg("测试测试");
            ingestNotify.setAssetId("ceshi1234");
            ingestNotify.setSequence(sequence);
            IngestNotifyResponse response = service.ingestNotifyDeal(ingestNotify);
            System.out.println(response.getAMSID());

        } catch (javax.xml.rpc.ServiceException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
