package example.other;

import com.wh.client.heilongjiang.CPImportServiceImplServiceLocator;
import com.wh.client.heilongjiang.CSPResult;
import com.wh.client.heilongjiang.CpImportService;

public class HelloWorldClient {
  public static void main(String[] argv) {
      try {
          CPImportServiceImplServiceLocator locator = new CPImportServiceImplServiceLocator();
          CpImportService service = locator.getcpImportServicePort();
          // If authorization is required
          //((CPImportServiceImplServiceSoapBindingStub)service).setUsername("user3");
          //((CPImportServiceImplServiceSoapBindingStub)service).setPassword("pass3");
          // invoke business method
          String fipUrl =
                  "ftp://hljyy:wasu2019@10.20.30.50:21/test/add_HeiLongJiangXMT_CP23010020191112068109_4f4268db780849438608704ff7048f16.xml";
          System.out.println("开始执行main方法 fipUrl "+fipUrl);
          CSPResult csp = service.execCmd("WASUHLGXMT","HQY","6308913",
                  fipUrl);
          System.out.println(" result : "+csp.getResult());
      } catch (javax.xml.rpc.ServiceException ex) {
          ex.printStackTrace();
      } catch (java.rmi.RemoteException ex) {
          ex.printStackTrace();
      }
  }

//    public static void main(String[] args) {
//        HelloWorldServiceLocator locator = new HelloWorldServiceLocator();
//        try {
//            HelloWorld_PortType portType = locator.getHelloWorld();
//            portType.sayHelloWorldFrom("ceshi");
//
//        } catch (ServiceException | RemoteException e) {
//            e.printStackTrace();
//        }
//    }
}
