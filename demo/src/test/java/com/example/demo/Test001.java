package com.example.demo;


import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

public class Test001 {

    class User {
        String id;
        String name ;
        String age;

        public User(String id, String name, String age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", age='" + age + '\'' +
                    '}';
        }
    }
    public static void main(String[] args) {
//        String str = "2019年10月";
////        System.out.println(str.substring(0,str.indexOf("年")));
////        System.out.println(str.substring(str.indexOf("年")+1,str.indexOf("月")));
//        str = str.replace("1","2");
//        str = str.replace("年","nian");
//        System.out.println(str);
//        System.out.println("ncskfjsl.xlsx".matches("^nc.*\\.xlsx$"));
//        System.out.println("nq_ncskfjsl.xlsx".matches("^nc.*\\.xlsx$|^nq_nc.*\\.xlsx$"));
//        System.out.println("nq_fdasfsdaskfjsl.xlsx".matches("^nc.*\\.xlsx$|^nq_nc.*\\.xlsx$"));
//        System.out.println("nq_nc20191108.xlsx".matches("^nc.*\\.xlsx$|^nq_nc.*\\.xlsx$"));

//        String title = "《野生厨房2》超长预告：林依轮汪苏泷上车，汪涵领衔全新野>\n" +
//                "生家族出发啦！";
//        if(title!=null){
//            // 去除英文单双引号
//            title = title.replace("\"","");
//            title = title.replace("'","");
//            // 去掉回车和换行
//            title = title.replace("\n","").replace("\r","");
//        }
//        System.out.println(title);

//        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH )+1;
        int month = cal.get(Calendar.MONTH );

        month=2;

        String tpr_tabName = "t_publish_record_";
        String tar_tabName = "t_asset_record_";
        String current_year = String.valueOf(year);
        for (int i = month; i >(month-3) ; i--) {
            String c = "";
            int current_month = i;

            if(i<=0){
                current_month=i+12;
            }
            if(i<=0){
                current_year = String.valueOf(year-1);
            }
            if(current_month<10){
                c="0";
            }
            System.out.println(tpr_tabName+current_year+c+current_month);
            System.out.println(tar_tabName+current_year+c+current_month);
        }

    }

    @Test
    public void test002(){
//        String str1 = "^(/wasteCollection/).*";
        String str1 = "^(/enter_place/).*";
//        String str = "\\?|\\*";
        Pattern pattern = Pattern.compile(str1);
        System.out.println(pattern.pattern());
        Matcher m = pattern.matcher("/enter_place/list");
        System.out.println(m.find());
    }
    @Test
    public void test003(){
//        String str1 = "^(/wasteCollection/).*";
        String str1 = "^(/enter_place/).*";
//        String str = "\\?|\\*";
        Pattern pattern = Pattern.compile(str1);
        System.out.println(pattern.pattern());
        Matcher m = pattern.matcher("/enter_place/list");
        System.out.println(m.find());
    }@Test
    public void test004(){
        String str1 = ".*";
        Pattern pattern = Pattern.compile(str1);
        Matcher m = pattern.matcher("multipart/alternative;\n" +
                "\ttype=\"text/html\";\n" +
                "\tboundary=\"----=_NextPart_004_1576045641194\"");
        System.out.println(m.find());
    }
    private static final String name_delimiter = "、";
    private static final String regex = "^" + name_delimiter + "*|" + name_delimiter + "*$";
    @Test
    public void test005(){
        System.out.println((name_delimiter+"wanghao"+name_delimiter).replaceAll(regex,""));

        System.out.println("、wang、hao、".replaceAll("^"+name_delimiter,""));
        System.out.println("、wang、hao、".indexOf("^、"));
        System.out.println(name_delimiter+"王浩"+name_delimiter+"fdasfs"+name_delimiter
                .replaceAll(regex,""));
    }

    @Test
    public void test006(){
        System.out.println("Destroy_".split("_")[0]);
    }
    @Test
    public void test007(){
        List<String> list = new ArrayList<>();
        list.add("新建文件夹2");
        list.add("tomcat-vod-jiangXiYX");
//        list.add("tomcat-vod-jiangXiYX.tar.gz");

        System.out.println(list.contains("tomcat-vod-jiangXiYX.tar.gz".replace(".tar","").replace(".gz","")));
//        File file =new File("D:\\upload");
//        File[] directoryArr = file.listFiles();
//        for (File file2 :
//                directoryArr) {
//            System.out.println(new ArrayList(Arrays.asList(file2.listFiles())));
//        }
    }

    @Test
    public void test008(){
        System.out.println("15825331057971221asdfsa15825331057971221asdfsa".matches("\\d+"));
        System.out.println("null".matches("\\d+"));
    }
    @Test
    public void test009(){
        boolean b =false;
        int i = 0;
        do {
            System.out.println(i);
            b=true;
            i++;
        }while (i<3 && !b );
    }




    }
