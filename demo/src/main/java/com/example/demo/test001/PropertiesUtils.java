package com.example.demo.test001;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class PropertiesUtils {
    private static Logger log = LoggerFactory.getLogger(PropertiesUtils.class);
    private static Properties p ;

    static {
        try {
            p = new Properties();
            p.load(new InputStreamReader(
                    PropertiesUtils.class.getClassLoader().getResourceAsStream("utils.properties"),"UTF-8"));
        } catch (Exception e) {
            log.error("加载配置文件出错");
        }
    }

    public static void main(String[] args)  {
        System.out.println(PropertiesUtils.getPropertiesByKey("jsonOutFilePath"));
    }
    public void getPath() throws IOException {
        //当前项目下路径
        File file = new File("");
        String filePath = file.getCanonicalPath();
        System.out.println(filePath);

        //当前项目下xml文件夹
        File file1 = new File("");
        String filePath1 = file1.getCanonicalPath()+File.separator+"xml\\";
        System.out.println(filePath1);

        //获取类加载的根路径
        File file3 = new File(this.getClass().getResource("/").getPath());
        System.out.println(file3);

        //获取当前类的所在工程路径
        File file4 = new File(this.getClass().getResource("").getPath());
        System.out.println(file4);

        //获取所有的类路径 包括jar包的路径
        System.out.println(System.getProperty("java.class.path"));

        String path = System.getProperty("user.dir");
        System.out.println(path);
    }

    public static String getPropertiesByKey(String key){
        if(StringUtils.isBlank(key)){
            return null;
        }
        return  p.getProperty(key.trim());
    }

    public static String getPropertiesByKey(String key,String def){
        if(StringUtils.isBlank(key)){
            return def;
        }
        String val = p.getProperty(key.trim());
        if(StringUtils.isBlank(val)){
            return def;
        }
        return  val;
    }


    public static void setPropertiesByKey(String key,String val){
        if(StringUtils.isBlank(key)){
            return ;
        }
        p.setProperty(key.trim(),val);
    }







}
