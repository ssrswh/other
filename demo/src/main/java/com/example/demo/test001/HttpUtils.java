package com.example.demo.test001;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class HttpUtils {

    /**
     * Connection conn =  Jsoup.connect("http://live.win007.com/NewTop.js?ver=9").ignoreContentType(true);
     *         String body = conn.execute().body();
     *         System.out.println(body);
     *         ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
     *         String func = "var return2 = function(){ "+body+" return JSON.stringify(winLoc)}";
     *         engine.eval(func);
     *         Invocable inv = (Invocable) engine;
     *         String result = (String) inv.invokeFunction("return2");
     *         System.out.println(result);
     *
     */


    /**
     * 发送get请求
     * @param url
     * @return
     */
    public static String sendGet(String url){
        try {
            CloseableHttpClient client = null;
            CloseableHttpResponse response = null;
            try {
                HttpGet httpGet = new HttpGet(url);

                client = HttpClients.createDefault();
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity);
                return result;
            } finally {
                if (response != null) {
                    response.close();
                }
                if (client != null) {
                    client.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



}
