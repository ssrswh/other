package com.example.demo.test001;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Test10086 {


    private static Gson gson = new Gson();
    private static String jsonAndExcelOutPath = PropertiesUtils.getPropertiesByKey("jsonOutFilePath");
    public static void jsonToExcel(String sendMail) throws ParseException, IOException {
        // 获取json数据
        String json = HttpUtils.sendGet("http://2019ncov.nosugartech.com/data.json?439183");
        Map jsonMap = gson.fromJson(json, HashMap.class);
        List<HashMap<String,String>> jsonList = gson.fromJson(gson.toJson(jsonMap.get("data")),List.class);
        List<Map<String,String>> resultJson = new ArrayList<>();
        for (Map map :
                jsonList) {
            Map<String,String> map1 = new HashMap<>();
            map1.put("日期",map.get("t_date").toString());
            map1.put("交通类型",getType(map.get("t_type").toString()));
            map1.put("车次/车牌/航班号/场所名",map.get("t_no").toString());
            map1.put("车厢",map.get("t_no_sub").toString());
            map1.put("出发站",map.get("t_pos_start").toString());
            map1.put("到达站",map.get("t_pos_end").toString());
            map1.put("车次附加描述",String.valueOf(map.get("t_memo")));
            map1.put("开始时间",getDate(map.get("t_start").toString()));
            map1.put("结束时间",getDate(map.get("t_end").toString()));
            map1.put("线索来源",map.get("who").toString());
            map1.put("提交时间",getDate(map.get("created_at").toString()));
            map1.put("事件备注","");
            resultJson.add(map1);
        }
        // 备份json数据
        FileUtils.writeStringToFile(new File(jsonAndExcelOutPath+
                new SimpleDateFormat("yyyyMMdd-HH-mm-ss").format(new Date()) +".json"),gson.toJson(resultJson));
        // 导出Excel
        String filePath = Json4Excel.jsonToExcel(gson.toJson(resultJson),null);

        // 发送邮件
        SendMail.sendMail(sendMail,filePath,"新型冠状病毒感染的肺炎同行程数据","数据在附件Excel中，如果没有Excel附件，请联系！！");
    }



    public static List<Map<String,String>> getJson() throws ParseException {
        // 获取json数据
        String json = HttpUtils.sendGet("http://2019ncov.nosugartech.com/data.json?439183");
        Map jsonMap = gson.fromJson(json, HashMap.class);
        List<HashMap<String,String>> jsonList = gson.fromJson(gson.toJson(jsonMap.get("data")),List.class);
        List<Map<String,String>> resultJson = new ArrayList<>();
        for (Map map :
                jsonList) {
            Map<String,String> map1 = new HashMap<>();
            map1.put("日期",map.get("t_date").toString());
            map1.put("交通类型",getType(map.get("t_type").toString()));
            map1.put("车次/车牌/航班号/场所名",map.get("t_no").toString());
            map1.put("车厢",map.get("t_no_sub").toString());
            map1.put("出发站",map.get("t_pos_start").toString());
            map1.put("到达站",map.get("t_pos_end").toString());
            map1.put("车次附加描述",String.valueOf(map.get("t_memo")));
            map1.put("开始时间",getDate(map.get("t_start").toString()));
            map1.put("结束时间",getDate(map.get("t_end").toString()));
            map1.put("线索来源",map.get("who").toString());
            map1.put("提交时间",getDate(map.get("created_at").toString()));
            map1.put("事件备注","");
            resultJson.add(map1);
        }
        return resultJson;
    }

    public static String exportToExcel() throws ParseException, IOException {
        // 获取json数据
        String json = HttpUtils.sendGet("http://2019ncov.nosugartech.com/data.json?439183");
        Map jsonMap = gson.fromJson(json, HashMap.class);
        List<HashMap<String, String>> jsonList = gson.fromJson(gson.toJson(jsonMap.get("data")), List.class);
        List<Map<String, String>> resultJson = new ArrayList<>();
        for (Map map :
                jsonList) {
            Map<String, String> map1 = new HashMap<>();
            map1.put("日期", map.get("t_date").toString());
            map1.put("交通类型", getType(map.get("t_type").toString()));
            map1.put("车次/车牌/航班号/场所名", map.get("t_no").toString());
            map1.put("车厢", map.get("t_no_sub").toString());
            map1.put("出发站", map.get("t_pos_start").toString());
            map1.put("到达站", map.get("t_pos_end").toString());
            map1.put("车次附加描述", String.valueOf(map.get("t_memo")));
            map1.put("开始时间", getDate(map.get("t_start").toString()));
            map1.put("结束时间", getDate(map.get("t_end").toString()));
            map1.put("线索来源", map.get("who").toString());
            map1.put("提交时间", getDate(map.get("created_at").toString()));
            map1.put("事件备注", "");
            resultJson.add(map1);
        }
        // 备份json数据
        FileUtils.writeStringToFile(new File(jsonAndExcelOutPath +
                new SimpleDateFormat("yyyyMMdd-HH-mm-ss").format(new Date()) + ".json"), gson.toJson(resultJson));
        // 导出Excel
        String filePath = Json4Excel.jsonToExcel(gson.toJson(resultJson), null);
        return filePath;
    }


    public static String getDate(String utc) throws ParseException {
        // 最新代码2020年2月7日16:24:22  没有时差
//        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat df2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df2.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = df2.parse(utc);
        return utc;
    }
    public static String getType(String typeNum){
        switch (typeNum){
            case "1.0":
                return "飞机";
            case "2.0":
                return "火车";
            case "3.0":
                return "地铁";
            case "4.0":
                return "长途客车/大巴";
            case "5.0":
                return "公交车";
            case "6.0":
                return "出租车";
            case "7.0":
                return "轮船";
            case "8.0":
                return "其它公共场所";
            default:
                return typeNum;
        }
    }
}
