package com.example.demo.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    @RequestMapping("/hi")
    public String hi (){
        return "hello world! ";
    }
    @RequestMapping("/hello")
    public String hello (){
        return "this is my 1st java application ";
    }
    @RequestMapping("/hello/{name}")
    public String helloName (@PathVariable(value = "name") String name){
        return " hello " + name + "!";
    }
    @RequestMapping("/nihao/{name}")
    public String niHao(@PathVariable(value = "name") String name) {
        return " hello " + name + "!";
    }

    @RequestMapping("/hi/{name}")
    public String hi(@PathVariable(value = "name") String name) {
        String myName = name + " i am good!";
        return "this is my 1st java application! " + myName;
    }
}
