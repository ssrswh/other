package com.example.demo.controller;

import com.example.demo.test001.Test10086;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@RequestMapping("/json4excel")
@RestController
public class Json4ExcelController {

    @RequestMapping("/send")
    public String send(String sendMail){
        if(StringUtils.isEmpty(sendMail)){
            return  "发送邮件地址为空";
        }

        try {
            Test10086.jsonToExcel(sendMail);
            return  "ok,发送【"+sendMail+  "】成功";
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  "导出失败";
    }

    @RequestMapping("/export")
    public String export(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ParseException {
        String filePath = Test10086.exportToExcel();
        File file = new File(filePath);
        if(file.exists()){
            // 配置文件下载
            response.setHeader("content-type", "application/octet-stream");
            response.setContentType("application/octet-stream");
            // 下载文件能正常显示中文
            response.setHeader("Content-Disposition", "attachment;filename="
                    +filePath.substring(filePath.lastIndexOf(File.separatorChar)+1));
            // 实现文件下载
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try{
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                System.out.println("Download the song successfully!");
            }catch (Exception e){
                System.out.println("Download the song failed!");
            }finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return  "导出完成";
    }

    @RequestMapping("/getJson")
    public List<Map<String,String>> getJson()  {
        try {
            return Test10086.getJson();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
