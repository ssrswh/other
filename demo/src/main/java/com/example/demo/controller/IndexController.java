package com.example.demo.controller;

import com.example.demo.wasu.ShanDongReadLogSend2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
@RequestMapping("/index")
public class IndexController {

    @RequestMapping("/index")
    public String index() throws IOException {
        ShanDongReadLogSend2.scanLogSendUrl();
        return null;
    }
}
