/**
 * 
 */
package com.wasu.vod.controller;

import com.wasu.vod.utils.FtpUtil;
import com.wasu.vod.utils.SftpUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * vod服务
 * 
 * @author wangjian
 *
 */
public class VodController  {

	public static void main(String[] args) {
		VodController vodController = new VodController();
		String fileName = args[0];
		String ftpUrl=args[1];

		String fileContent=args[2];
		String encode="utf-8";
		int i = 0;
		boolean b = false;
		do {
			b = vodController.uploadFile(fileName,ftpUrl,fileContent,encode);
			System.out.println("开始上传 2次数：" +i+" 是否成功："+b);
			i++;
		}while ( !b && i<3);

	}

	/**
	 * 上传文件
	 * @param filename
	 * @param filePath
	 * @param fileContent
	 * @return
	 */
	public boolean uploadFile(String filename, String ftpURL, String fileContent, String encode){
		try {
			fileContent = URLDecoder.decode(fileContent, "utf-8");
			String ip = "";
			int port = 21;
			String userName = "";
			String  password = "";
			String rootPath = "/";
			String ftpType = ftpURL.substring(0,ftpURL.indexOf("://")).toUpperCase();
			ftpURL = ftpURL.substring(ftpURL.indexOf("//") + 2,ftpURL.length());//去掉ftp://字样
			userName = ftpURL.substring(0,ftpURL.indexOf(":"));
			ftpURL =  ftpURL.replace(userName + ":", "");//去掉username
			password = ftpURL.substring(0,ftpURL.indexOf("@"));
			ftpURL =  ftpURL.replace(password + "@", "");//去掉password
			if(ftpURL.contains(":")){
				ip = ftpURL.substring(0,ftpURL.indexOf(":"));
				ftpURL =  ftpURL.replace(ip + ":", "");//去掉ip
				if(ftpURL.contains("/")){
					port = Integer.parseInt(ftpURL.substring(0,ftpURL.indexOf("/")));
					rootPath =  ftpURL.replace(String.valueOf(port), "");//去掉端口号
				}
				else{
					port = Integer.parseInt(ftpURL.substring(0,ftpURL.length()));
					//rootpath默认
				}
			}
			else if(ftpURL.contains("/")){
				ip = ftpURL.substring(0,ftpURL.indexOf("/"));
				//端口号默认
				rootPath =  ftpURL.replace(ip, "");//去掉ip
			}
			else {
				//最后一级，port和rootpath默认
				ip = ftpURL.substring(0,ftpURL.length());
			}

			if(!rootPath.endsWith("/")){
				rootPath = rootPath + "/";
			}
			if("FTP".equals(ftpType)){
				System.out.println(String.format("打印日志链接信息： ip:%s,port:%s,userName:%s,password:%s",
						ip,port,userName,password));
                System.out.println(String.format("打印日志目录信息： rootPath:%s,fileName:%s,fileContent:%s,encode:%s",
                        rootPath,filename,fileContent,encode));
				FtpUtil fileUtil = new FtpUtil(ip,port,userName,password);
				if (fileUtil.connect()) {
					System.out.println(" 链接成功");
					return fileUtil.putContent(rootPath + filename,
							fileContent, encode);
				} else {
					return false;
				}
			}
			else {
				SftpUtil sfileUtil = new SftpUtil(ip,port,userName,password);
				if(sfileUtil.connect()){
					return sfileUtil.putContent(rootPath + filename, fileContent, encode);
				}
				else {
					return false;
				}
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}
	}


}
